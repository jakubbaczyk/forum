/* Klasy własne (dane) */

class User {
    constructor(id,login,pass,email){
        this.id = id;
        this.login = login;
        this.pass = pass;
        this.email = email;
    }
}
class Topic {
    constructor(id,title,context,authorid){
        this.id = id;
        this.title = title;
        this.context = context;
        this.authorid = authorid;
    }
}
class Post {
    constructor(id,topic,context,authorid){
        this.id = id;
        this.topic = topic;
        this.context = context;
        this.authorid = authorid;
    }
}
var user = new Array();
user[1] = new User(1,"Marek","marek","marek@mail.com");
user[2] = new User(2,"Kacper","1234","kac.perr@email.pl");
user[3] = new User(3,"Adam","admin1","adm@email.com");
user[4] = new User(4,"Wojtek","woj","mrkk@mail.pl");
var topic = new Array();
topic[1] = new Topic(1,"Witajcie!","W tym temacie możecie się witać z innymi użytkownikami. Zatem witam Wam serdecznie!",1);
topic[2] = new Topic(2,"Klasa w JS","Wie ktoś dlaczego nie mogę mieć znaczników Private w JS? Potrzebne mi, bo to przecież mega tajne dane są, których nie widać z poziomu przeglądarki.",3);
var post = new Array();
post[1] = new Post(1,2,"Mi w sumie też by się to przydało.",4);
post[2] = new Post(2,1,"Cześć, jestem Kacper i lubię pisać kod.",2);
post[3] = new Post(3,2,"Panowie ale przecież te dane są widoczne z poziomu źródła strony, więc jaki sens miałoby \"ukrywanie\" ich z poziomu dostępu do klas?",2);
post[4] = new Post(4,1,"Siemka, mam na imię Wojtek i tak sobie tutaj patrzę co mogę się pouczyć od Was ;)",4);
post[5] = new Post(5,2,"Ok, nie było tematu.",3);

/* Klasy Reacta (Elementy wyglądu strony) */

const e = React.createElement;

class Author extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var id = this.props.authorid;
        var login = user[id].login;
        return(
            React.createElement("a", { className: "login", href: "profile.html?id="+id} ,login)
        );
    }
}

class Title_topics extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var id = this.props.topicid;
        var title = topic[id].title;
        return(
            React.createElement("a", { className: "title", href: "topic.html?id="+id } ,title)
        );
    }
}

class Context_topics extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var id = this.props.topicid;
        var context = topic[id].context;
        return(
            React.createElement("span", { className: "texts" } ,context)
        );        
    }
}

class Context_post extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var id = this.props.postid;
        var context = post[id].context;
        return(
            React.createElement("span", { className: "texts" } ,context)
        );        
    }
}

class Topic_in extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var idtopic = this.props.topicid;
        var idauthor = topic[idtopic].authorid;
        return([React.createElement("div", { className: "topic_title", 'data-id': idtopic}, ""),
                React.createElement("div", { className: "author", 'data-id': idauthor}, ""),
                React.createElement("div", { className: "topic_context", 'data-id': idtopic}, "")]
        );
    }
}

class Topic_main extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var idtopic = this.props.topicid;
        var idauthor = topic[idtopic].authorid;
        var postsnumber = 1;
        for(var i=1;i<post.length;i++){
            if(idtopic==post[i].topic){
                postsnumber++;
            }
        }
        return([React.createElement("div", { className: "author", 'data-id': idauthor}, ""),
                React.createElement("div", { className: "topic_title", 'data-id': idtopic}, ""),
                React.createElement("div", { className: "posts_number"}, postsnumber)]
        );
    }
}

class Context extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        var result = new Array();
        var page = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
        if(page == "index.html"){
            result[0] = React.createElement("div", { className: "tab" }, 
                [React.createElement("div", { className: "tab1" }, "Autor wątku"),
                React.createElement("div", { className: "tab2" }, "Tytuł wątku"),
                React.createElement("div", { className: "tab3"  }, "Ilość postów")]
            );
            for(var i=topic.length-1;i>0;i--){
                result[i] = React.createElement("div", { className: "topic_main", 'data-id': i}, "");
            }
        }
        else if(page == "topic.html"){
            var url_str = window.location.href;
            var url = new URL(url_str);
            const id_current_topic = url.searchParams.get("id");
            if(id_current_topic < topic.length){
                result[0] = React.createElement("div", { className: "topic_in", 'data-id': id_current_topic}, "");
                var j=1;
                for(var i=1; i<post.length; i++){
                    if(post[i].topic==id_current_topic){
                        result[j] = React.createElement("div", { className: "author", 'data-id': post[i].authorid},"");
                        j++;
                        result[j] = React.createElement("div", { className: "post_context", 'data-id': i}, "");
                        j++;
                    }
                }
            }
            else {
                result = "Nie ma takiego tematu";
            }
        }
        else if(page == "profile.html"){
            var url_str = window.location.href;
            var url = new URL(url_str);
            const id_current_profile = url.searchParams.get("id");
            if(id_current_profile < user.length){
                var login = user[id_current_profile].login;
                var email = user[id_current_profile].email;
                result[0] = React.createElement("p", {className: "profile_welcome" }, "Witaj na profilu użytkownika "+login);
                result[1] = React.createElement("p", {className: "profile_data" }, "Adres email: "+email);
            }
            else {
                result = "Podano niewłaściwy profil.";
            }
        }
        else if(page == "users.html"){
            result[0] = React.createElement("div", { className: "tab" }, 
            React.createElement("div", { className: "tab1" }, "Numer"),
            React.createElement("div", { className: "tab2" }, "Login z linkiem do profilu"),
            React.createElement("div", { className: "tab3" }, "Ilość postów")
            )
            var number_of_posts = new Array();
            for(var i=1; i<user.length; i++){
                number_of_posts[i] = 0;
            }
            for(var i=1; i<topic.length; i++){
                var aid = topic[i].authorid;
                number_of_posts[aid]++;
            }
            for(var i=1; i<post.length; i++){
                var aid = post[i].authorid;
                number_of_posts[aid]++;
            }
            for(var i=1; i<user.length; i++){
                result[i] = React.createElement("div", { className: "tab" }, 
                    React.createElement("div", { className: "tab1" }, i),
                    React.createElement("div", { className: "tab2" }, React.createElement("div", {      className: "author", 'data-id': i },"")),
                    React.createElement("div", { className: "tab3" }, number_of_posts[i])
                )
            }
        }
        else {
            result = "Nie ma takiej strony.";
        }
        return(result);
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return 'Witaj na forum.';
    }
}

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            React.createElement("div", { width: "100%" },
                React.createElement("p", { className: "footer"} , "Stopka forum."),
                React.createElement("p", { className: "footer"} , "Strona została stworzona przez Jakuba.")
            )
        );
    }
}

class MainMenu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return ([
            React.createElement("a", { href: 'index.html'} , "Strona główna"),
            React.createElement("a", { href: 'users.html'} , "Użytkownicy")]
        );
    }
}

/* Zpałenianie forum treścią */

document.querySelectorAll('#context')
  .forEach(domContainer => {
    ReactDOM.render(
        e(Context, { }),
        domContainer
    );
});

document.querySelectorAll('#header')
  .forEach(domContainer => {
    ReactDOM.render(
      e(Header, { }),
      domContainer
    );
});

document.querySelectorAll('#footer')
  .forEach(domContainer => {
    ReactDOM.render(
        e(Footer, { }),
        domContainer
    );
});

document.querySelectorAll('#menu')
    .forEach(domContainer => {
      ReactDOM.render(
        e(MainMenu, { }),
        domContainer
      );
});

/* Utworzenie i przypisanie elementów Reacta do szablonu strony */

document.querySelectorAll('.topic_in')
  .forEach(domContainer => {
    const topicid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Topic_in, { topicid: topicid }),
        domContainer
    );
});

document.querySelectorAll('.topic_main')
  .forEach(domContainer => {
    const topicid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Topic_main, { topicid: topicid }),
        domContainer
    );
});

document.querySelectorAll('.author')
  .forEach(domContainer => {
    const authorid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Author, { authorid: authorid }),
        domContainer
    );
});

document.querySelectorAll('.topic_title')
  .forEach(domContainer => {
    const topicid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Title_topics, { topicid: topicid }),
        domContainer
    );
});

document.querySelectorAll('.topic_context')
  .forEach(domContainer => {
    const topicid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Context_topics, { topicid: topicid }),
        domContainer
    );
});

document.querySelectorAll('.post_context')
  .forEach(domContainer => {
    const postid = parseInt(domContainer.dataset.id, 10);
    ReactDOM.render(
        e(Context_post, { postid: postid }),
        domContainer
    );
});